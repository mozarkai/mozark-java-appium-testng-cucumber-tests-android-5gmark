package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FullTestSteps extends BaseClass {
					
	@Given("I click on 'Full test' option")
	public void clickFullTest() {
		setup();
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/id_test_scenario_full")));
		driver.findElement(By.id("com.agence3pp:id/id_test_scenario_full")).click();		
	}
	
	@And("I click on 'Test' option")
	public void clickTest() {
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Here's an image or an icon\"]")).click();
	}
	
	@When("I click on 'Indoor' option")
	public void clickIndoor() {
		WebDriverWait wait1 = new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/test_bottom_sheet_1")));
		driver.findElement(By.id("com.agence3pp:id/test_bottom_sheet_1")).click();
	}
	
	@And("I wait till result page is displayed")
	public void waitForResult() {
		WebDriverWait wait2 = new WebDriverWait(driver, 240);
		wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Results\"]")));
	}
	
	@And("I navigate to Explanation tab to display full result")
	public void navigateToExplanation() {
		driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Explanations\"]")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Then("Results are printed")
	public void printResult() {
		String download = driver.findElement(By.xpath("//android.view.ViewGroup[1]/android.widget.TextView[2]")).getText();
		System.out.println("Download result is: "+download);
		String upload = driver.findElement(By.xpath("//android.view.ViewGroup[2]/android.widget.TextView[2]")).getText();
		System.out.println("Upload result is: "+upload);
		String stream = driver.findElement(By.xpath("//android.view.ViewGroup[3]/android.widget.TextView[2]")).getText();
		System.out.println("Stream result is: "+stream);
		String web = driver.findElement(By.xpath("//android.view.ViewGroup[4]/android.widget.TextView[2]")).getText();
		System.out.println("Web result is: "+web);
		driver.navigate().back();
		WebDriverWait wait4 = new WebDriverWait(driver,10);
		wait4.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.agence3pp:id/navigation_2")));
		
	}

}
